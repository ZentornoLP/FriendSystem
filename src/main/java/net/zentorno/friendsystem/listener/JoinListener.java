package net.zentorno.friendsystem.listener;

import net.zentorno.friendsystem.utils.Friend;
import net.zentorno.friendsystem.utils.ItemStackBuilder;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;

public class JoinListener implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent event){

        event.setJoinMessage(null);

        Friend friend = new Friend(event.getPlayer().getUniqueId(), true, true, new ArrayList<String>());

        friend.build();

        ItemStack itemStack = new ItemStackBuilder(Material.SKULL_ITEM).name("§5Friends").lore("§7Right Click to Open").build();

        event.getPlayer().getInventory().setItem(8, itemStack);

    }

}
