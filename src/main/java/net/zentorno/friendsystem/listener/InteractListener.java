package net.zentorno.friendsystem.listener;

import net.zentorno.friendsystem.FriendSystem;
import net.zentorno.friendsystem.utils.ItemStackBuilder;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class InteractListener implements Listener {

    @EventHandler
    public void onInteract(PlayerInteractEvent event){

        if(event.getItem().getType().equals(Material.SKULL_ITEM)){

            Inventory inventory = Bukkit.createInventory(null, 27, "§7Friends");

            for (int i = 0; i < FriendSystem.getPlayers().get(event.getPlayer().getUniqueId()).getFriends().size(); i++){

                OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(FriendSystem.getPlayers().get(event.getPlayer().getUniqueId()).getFriends().get(i));
                System.out.println(offlinePlayer.getName());
                ItemStack kopfi = new ItemStackBuilder(Material.SKULL_ITEM).owner(offlinePlayer.getName()).name(offlinePlayer.getName()).build();
                inventory.setItem(i, kopfi);

            }

            event.getPlayer().openInventory(inventory);

        }

    }

}
