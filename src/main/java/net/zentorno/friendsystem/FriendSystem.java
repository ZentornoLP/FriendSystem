package net.zentorno.friendsystem;

import lombok.Getter;
import net.zentorno.friendsystem.commands.FriendAddCMD;
import net.zentorno.friendsystem.listener.InteractListener;
import net.zentorno.friendsystem.listener.JoinListener;
import net.zentorno.friendsystem.utils.ConfigManager;
import net.zentorno.friendsystem.utils.Friend;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.IOException;
import java.util.HashMap;
import java.util.UUID;

public class FriendSystem extends JavaPlugin {

    @Getter
    public static FriendSystem instance;
    @Getter
    public static HashMap<UUID, Friend> players = new HashMap<UUID, Friend>();

    @Override
    public void onEnable() {

        instance = this;

        ConfigManager configManager = new ConfigManager();

        try {
            configManager.create();
            configManager.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        this.getCommand("friend").setExecutor(new FriendAddCMD());

        Bukkit.getPluginManager().registerEvents(new InteractListener(), this);
        Bukkit.getPluginManager().registerEvents(new JoinListener(), this);
    }

    @Override
    public void onDisable() {


    }
}
