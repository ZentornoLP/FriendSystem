package net.zentorno.friendsystem.commands;

import net.zentorno.friendsystem.FriendSystem;
import net.zentorno.friendsystem.utils.Friend;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class FriendAddCMD implements CommandExecutor {

    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {

        if(!(sender instanceof Player)){

            System.out.println("Nix Player");

            return false;
        }

        Player player = (Player) sender;

        if(!(args.length == 2)){

            player.sendMessage("§c/friend add/remove <Player>");

            return false;
        }

        if(args[0].equalsIgnoreCase("add")){

            OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(args[1]);

            System.out.println(FriendSystem.getPlayers().get(player.getUniqueId()).getFriends().size());
            if(!(FriendSystem.getPlayers().get(player.getUniqueId()).getFriends().contains(offlinePlayer.getUniqueId()))){

                Friend friend = FriendSystem.getPlayers().get(player.getUniqueId());
                friend.getFriends().add(offlinePlayer.getUniqueId().toString());
                friend.update();

                player.sendMessage("§aFreund §e" + args[1] + "§a hinzugefügt!");

                return false;
            }

            player.sendMessage("§cDu bist schon mit dem Typ befreundet!");

            return false;
        }

        if(args[0].equalsIgnoreCase("remove")){


            OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(args[1]);

            if(FriendSystem.getPlayers().get(player.getUniqueId()).getFriends().contains(offlinePlayer.getUniqueId().toString())){

                Friend friend = FriendSystem.getPlayers().get(player.getUniqueId());
                friend.getFriends().remove(offlinePlayer.getUniqueId().toString());
                friend.update();

                player.sendMessage("§aFreund §e" + args[1] + "§a removed!");

                return false;
            }

            player.sendMessage("§cDu bist schon mit dem Typ net befreundet!");

        }

        return false;
    }
}
