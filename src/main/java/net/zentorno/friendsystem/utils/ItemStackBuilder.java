package net.zentorno.friendsystem.utils;

import com.google.common.collect.ImmutableList;
import org.bukkit.Color;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.block.banner.Pattern;
import org.bukkit.block.banner.PatternType;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.*;
import org.bukkit.material.MaterialData;

import java.util.ArrayList;
import java.util.List;

public class ItemStackBuilder {
    protected ItemStack is;
    protected ItemMeta meta;

    public ItemStackBuilder(Material mat) {
        this(new ItemStack(mat));
    }

    public ItemStackBuilder(Material mat, short data) {
        this(new ItemStack(mat, 1, data));
    }

    public ItemStackBuilder(ItemStack is) {
        this.is = is;
        this.is = is.clone();
        this.meta = is.getItemMeta();
    }

    public ItemStackBuilder amount(int amount) {
        this.is.setAmount(amount);
        return this;
    }

    public ItemStackBuilder name(String name) {
        this.meta.setDisplayName(name);
        return this;
    }

    public ItemStackBuilder lore(String name) {
        List<String> lore = this.meta.getLore();
        if (lore == null) {
            lore = new ArrayList();
        }

        lore.add(name);
        this.meta.setLore(lore);
        return this;
    }

    public ItemStackBuilder owner(String name) {
        if (this.is.getType() == Material.SKULL_ITEM) {
            ((SkullMeta) this.meta).setOwner(name);
            return this;
        } else {
            throw new IllegalArgumentException("owner() only applicable for skull items");
        }
    }

    public ItemStackBuilder durability(int durability) {
        this.is.setDurability((short) durability);
        return this;
    }

    public ItemStackBuilder data(int data) {
        this.is.setData(new MaterialData((short) data));
        return this;
    }

    public ItemStackBuilder enchantment(Enchantment enchantment, int level) {
        this.meta.addEnchant(enchantment, level, true);
        return this;
    }

    public ItemStackBuilder enchantment(Enchantment enchantment) {
        this.meta.addEnchant(enchantment, 1, false);
        return this;
    }

    public ItemStackBuilder type(Material material) {
        this.is.setType(material);
        return this;
    }

    public ItemStackBuilder clearLore() {
        this.meta.setLore(ImmutableList.<String>of());
        return this;
    }

    public ItemStackBuilder color(Color color) {
        if (this.is.getType() != Material.LEATHER_BOOTS && this.is.getType() != Material.LEATHER_CHESTPLATE && this.is.getType() != Material.LEATHER_HELMET && this.is.getType() != Material.LEATHER_LEGGINGS) {
            throw new IllegalArgumentException("color() only applicable for leather armor!");
        } else {
            ((LeatherArmorMeta) this.meta).setColor(color);
            return this;
        }
    }

    public ItemStackBuilder author(String name) {
        if (this.is.getType() == Material.WRITTEN_BOOK) {
            ((BookMeta) this.meta).setAuthor(name);
            return this;
        } else {
            throw new IllegalArgumentException("owner() only applicable for written books!");
        }
    }

    public ItemStackBuilder page(String content, int page) {
        if (this.is.getType() == Material.WRITTEN_BOOK) {
            ((BookMeta) this.meta).setPage(page, content);
            return this;
        } else {
            throw new IllegalArgumentException("owner() only applicable for written books!");
        }
    }

    public ItemStackBuilder page(String content) {
        if (this.is.getType() == Material.WRITTEN_BOOK) {
            ((BookMeta) this.meta).addPage(content);
            return this;
        } else {
            throw new IllegalArgumentException("owner() only applicable for written books!");
        }
    }

    public ItemStackBuilder layer(PatternType type, DyeColor color) {
        if (this.is.getType() == Material.BANNER) {
            ((BannerMeta) this.meta).addPattern(new Pattern(color, type));
            return this;
        } else {
            throw new IllegalArgumentException("bannermeta() only applicable for banners!");
        }
    }

    public ItemStackBuilder banner(List<Pattern> patterns) {
        if (this.is.getType() == Material.BANNER) {
            ((BannerMeta) this.meta).setPatterns(patterns);
            return this;
        } else {
            throw new IllegalArgumentException("bannermeta() only applicable for banners!");
        }
    }

    public ItemStackBuilder flag(ItemFlag flag) {
        this.meta.addItemFlags(flag);
        return this;
    }

    public ItemStackBuilder glow() {
        this.enchantment(Enchantment.DURABILITY);
        this.flag(ItemFlag.HIDE_ENCHANTS);
        return this;
    }

    public ItemStack build() {
        this.is.setItemMeta(this.meta);
        return this.is;
    }
}