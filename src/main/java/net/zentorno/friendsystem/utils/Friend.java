package net.zentorno.friendsystem.utils;

import lombok.Getter;
import net.zentorno.friendsystem.FriendSystem;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

@Getter
public class Friend {

    private UUID uuid;
    private Boolean friendrequests;
    private Boolean partyrequests;
    private List<String> friends;

    public Friend(UUID uuid){

        this.uuid = uuid;

    }

    public Friend(UUID uuid, Boolean friendrequests, Boolean partyrequests, List<String> friends){

        this.uuid = uuid;
        this.friendrequests = friendrequests;
        this.partyrequests = partyrequests;
        this.friends = friends;

    }

    public void update(){

        FriendSystem.getPlayers().remove(this.uuid);

        FriendSystem.getPlayers().put(this.uuid, this);

        File friends = new File(FriendSystem.getInstance().getDataFolder() + "/friends.yml");
        File settings = new File(FriendSystem.getInstance().getDataFolder() + "/settings.yml");

        YamlConfiguration friendConfig = YamlConfiguration.loadConfiguration(friends);
        YamlConfiguration settingsConfig = YamlConfiguration.loadConfiguration(settings);

        settingsConfig.set("Settings." + this.uuid + ".FriendRequests", this.friendrequests);
        settingsConfig.set("Settings." + this.uuid + ".PartyRequests", this.partyrequests);

        friendConfig.set("Friends." + this.uuid, this.friends);

        try {
            settingsConfig.save(settings);
            friendConfig.save(friends);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Boolean exists(){

        File player = new File(FriendSystem.getInstance().getDataFolder() + "/players.yml");
        YamlConfiguration playerConfig = YamlConfiguration.loadConfiguration(player);

        if(!(playerConfig.getStringList("Players").contains(this.uuid))){

            return false;

        } else {

            return true;

        }
    }

    public void build(){

        File friends = new File(FriendSystem.getInstance().getDataFolder() + "/friends.yml");
        File player = new File(FriendSystem.getInstance().getDataFolder() + "/players.yml");
        File settings = new File(FriendSystem.getInstance().getDataFolder() + "/settings.yml");

        YamlConfiguration playerConfig = YamlConfiguration.loadConfiguration(player);
        YamlConfiguration friendConfig = YamlConfiguration.loadConfiguration(friends);
        YamlConfiguration settingsConfig = YamlConfiguration.loadConfiguration(settings);

        List<String> test = playerConfig.getStringList("Players");
        test.add(this.uuid.toString());

        playerConfig.set("Players", test);
        friendConfig.set("Friends." + this.uuid, this.friends);
        settingsConfig.set("Settings." + this.uuid + ".FriendRequests", this.friendrequests);
        settingsConfig.set("Settings." + this.uuid + ".PartyRequests", this.partyrequests);

        try {
            settingsConfig.save(settings);
            friendConfig.save(friends);
            playerConfig.save(player);
        } catch (IOException e) {
            e.printStackTrace();
        }

        FriendSystem.getPlayers().put(this.uuid, this);
    }

}
