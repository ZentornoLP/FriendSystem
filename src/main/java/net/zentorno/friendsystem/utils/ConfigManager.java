package net.zentorno.friendsystem.utils;

import net.zentorno.friendsystem.FriendSystem;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

public class ConfigManager {

    public ConfigManager(){


    }

    public void create() throws IOException {

        File directory = FriendSystem.getInstance().getDataFolder();

        if(!(directory.exists())){

            directory.mkdirs();

        }

        File friends = new File(FriendSystem.getInstance().getDataFolder() + "/friends.yml");
        File player = new File(FriendSystem.getInstance().getDataFolder() + "/players.yml");
        File settings = new File(FriendSystem.getInstance().getDataFolder() + "/settings.yml");

        if(!(friends.exists())){

            friends.createNewFile();

        }
        if(!(player.exists())){

            player.createNewFile();

        }
        if(!(settings.exists())){

            settings.createNewFile();

        }
    }

    public void load() throws IOException {



        File friends = new File(FriendSystem.getInstance().getDataFolder() + "/friends.yml");
        File player = new File(FriendSystem.getInstance().getDataFolder() + "/players.yml");
        File settings = new File(FriendSystem.getInstance().getDataFolder() + "/settings.yml");

        YamlConfiguration configuration = YamlConfiguration.loadConfiguration(player);
        YamlConfiguration friendConfig = YamlConfiguration.loadConfiguration(friends);
        YamlConfiguration settingsConfig = YamlConfiguration.loadConfiguration(settings);

        List<String> players = configuration.getStringList("Players");

        for (int i = 0; i < players.size(); i++){

            Friend friend = new Friend(UUID.fromString(players.get(i)),
                    settingsConfig.getBoolean("Settings." + UUID.fromString(players.get(i)) + ".FriendRequests"),
                    settingsConfig.getBoolean("Settings." + UUID.fromString(players.get(i)) + ".PartyRequests"),
                    friendConfig.getStringList("Friends." + UUID.fromString(players.get(i))));

            FriendSystem.getPlayers().put(UUID.fromString(players.get(i)), friend);

        }
    }

}
